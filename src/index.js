const { app, BrowserWindow, ipcMain, Menu, dialog } = require("electron");
const path = require("path");
const FtpDeploy = require("ftp-deploy");

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require("electron-squirrel-startup")) {
  // eslint-disable-line global-require
  app.quit();
}

const createWindow = () => {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 900,
    height: 640,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, "index.html"));

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.

function getDate() {
  return new Date().toLocaleString();
}

ipcMain.on("deploy", async (event, arg) => {
  const ftp = new FtpDeploy();

  ftp.on("uploaded", function (data) {
    // console.log(data); // same data as uploading event
    event.sender.send(
      "log",
      `${getDate()} ${(
        (data.transferredFileCount / data.totalFilesCount) *
        100
      ).toFixed(0)}% ${data.filename}`
    );
  });
  ftp.on("upload-error", function (data) {
    console.log(data); // data will also include filename, relativePath, and other goodies
    event.sender.send("log", `${getDate()} 上传错误！${data.err} `);
  });

  for (let i = 0; i < arg.length; i++) {
    const config = arg[i];

    try {
      await ftp.deploy({
        ...config,
        localRoot: config.localRoot.replace(/\\/g, "/"),

        include: ["*", "**/*"],
        forcePasv: true,
      });
      event.sender.send("log", `${getDate()} 任务${i + 1}已上传成功`);
    } catch (err) {
      console.log(err);
      event.sender.send("log", `${getDate()} 错误！${err.message} `);
    }
  }

  event.sender.send("log", "done");
});

function about() {
  dialog.showMessageBox({
    message: "一个批量目录上传的小工具\n摆脱多服务器登录和手动拖文件的烦恼",
  });
}

const menu = Menu.buildFromTemplate([
  {
    label: "关于",
    click: about,
  },
]);

Menu.setApplicationMenu(menu);
